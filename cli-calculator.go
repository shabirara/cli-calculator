package main

import (
	"flag"
	"fmt"
)

func add(first int64, second int64) int64 {
	return first + second
}

func substract(first int64, second int64) int64 {
	return first - second
}

func multiply(first int64, second int64) int64 {
	return first * second
}

func div(first int64, second int64) int64 {
	return first / second
}

func modulo(first int64, second int64) int64 {
	return first % second
}

func main() {
	var first int64
	var second int64
	var op string
	var sum int64

	flag.Int64Var(&first, "first", 0, `-first=0"`)
	flag.Int64Var(&second, "second", 0, `-second=0"`)
	flag.StringVar(&op, "op", "", `-op=""`)

	flag.Parse()

	switch op {
	case "+":
		sum = add(first, second)
	case "-":
		sum = substract(first, second)
	case "*":
		sum = multiply(first, second)
	case "/":
		sum = div(first, second)
	case "%":
		sum = modulo(first, second)
	}

	fmt.Printf("Result of %d %s %d = %d \n", first, op, second, sum)

}
